import java.util.*;

/** Container class to different classes, makes the whole
 * set of classes one class formally.
 * @author of the original skeleton: jpoial
 *   - https://bitbucket.org/itc_algorithms/home6/
 * @author jeekim616 adaptions and developments
 */
public class GraphTask {
    /**
     * A fake main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * The actual main method to run tests and examples
     * @author jeekim616
     */
    public void run() {
        int verticesInGraph = 6; // don't modify here! optimal for tests
        int dualArcsInGraph = 9; // don't modify here! optimal for tests

        // TESTS - the console screen size is the limiting factor;
        boolean tooSmallScreen = false;
        if (verticesInGraph >= 24) tooSmallScreen = true;

        if (!tooSmallScreen) {
            run_test_00(verticesInGraph, dualArcsInGraph); // orig
            run_test_01(verticesInGraph, dualArcsInGraph); // count V
            run_test_02(verticesInGraph, dualArcsInGraph); // count A
            run_test_03(verticesInGraph, dualArcsInGraph); // conversion and isomorphism
            run_test_04(verticesInGraph, dualArcsInGraph); // isomorphism after 2x conversion
            run_test_05(verticesInGraph, dualArcsInGraph); // size and elements of a squared graph
            run_test_06(verticesInGraph, dualArcsInGraph); // verify the two methods are equivalent
        }
        verticesInGraph = 2000; // modify HERE!
        dualArcsInGraph = 3000; // modify HERE!

        //  ===== Two calculation methods in comparison:

        // method 1, SLOW (30sec) - multiplication of the adjacency matrix onto itself
        // NB! It is too slow to launch with n=2000 m=3000 (25 secs > timeout 20 s)
        calculate_PowerOfAGraph_viaMatrixMult(verticesInGraph, dualArcsInGraph);

        // method 2, FAST ( 5sec) - Main achievement of the work
        // Squaring the graph by WFI style BFS traversal
        calculate_PowerOfAGraph_viaBFS(verticesInGraph, dualArcsInGraph);
    }

    /**
     * A formal method to print a HoRizontal separation line for test sequences
     */
    public void hr() {
        System.out.println("=================================================================");
    }

    /**
     * A wrapper method to hide the actual test sequence away from the main code
     * @param n number of graph vertices to be generated for the particular test
     * @param m number of graph arcs to be generated for the particular test
     */
    public void run_test_00(int n, int m) {
        hr();
        System.out.println("TEST00: An original (random) graph as provided by the skeleton:");
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(n, m);
        System.out.println(g);
    }

    /**
     * A wrapper method to hide the actual test sequence away from the main code
     * @param n number of graph vertices to be generated for the particular test
     * @param m number of graph arcs to be generated for the particular test
     */
    public void run_test_01(int n, int m) {
        hr();
        System.out.println("TEST01: method countOfVertices() will count the number of Vertices.");
        Graph g11 = new Graph("G11");
        g11.createRandomSimpleGraph(n, m);
        int t = g11.countOfVertices();
        System.out.println("\tThe count of Vertices for the graph " + g11.id + " is: " + t + ",");
        System.out.println("\tA conclusion: the adjacency matrix must be of m*m size: " + t + "*" + t + "\n");
    }

    /**
     * A wrapper method to hide the actual test sequence away from the main code
     * @param n number of graph vertices to be generated for the particular test
     * @param m number of graph arcs to be generated for the particular test
     */
    public void run_test_02(int n, int m) {
        hr();
        System.out.println("TEST02: counting the number or Arcs by our method countOfArcsTotal()");
        System.out.println("\tby a simple BFS style traversal over Vertices and Arcs:\n");
        Graph g12 = new Graph("G12");
        g12.createRandomSimpleGraph(n, m);
        int z = g12.countOfArcsTotal();
        System.out.println("\n...   total Arc count in this directionless graph is " + z + ".");
        System.out.println("...   said otherwise: there are " + z / 2 + " double Arcs.\n");
    }

    /**
     * A wrapper method to hide the actual test sequence away from the main code
     * @param n number of graph vertices to be generated for the particular test
     * @param m number of graph arcs to be generated for the particular test
     */
    public void run_test_03(int n, int m) {
        hr();
        System.out.println("TEST03: creating an adj-list graph, converting it to adj-matrix");
        System.out.println("\t\tformat and back to the adj-list representation");
        System.out.println("\t\tNB! There will be 4 equivalent representations visible:\n");
        Graph g13 = new Graph("G13");
        g13.createRandomSimpleGraph(n, m);
        System.out.println(g13);
        SquareMatrix ma = new SquareMatrix();
        ma.setSize(n);
        ma.setId(g13.id);
        ma.content = g13.createAdjMatrix();
        System.out.println(ma.id);
        ma.printOut("content");
        System.out.println();
        Graph g14 = ma.toGraph("content");
        System.out.println("\t\tZero's were omitted b/c they describe no Arc.");
        System.out.println(g14);
        System.out.println("The conclusion (regretfully a manual/visual one):");
        System.out.println("\t\tall the representations are isomorphic!\n");
    }

    /**
     * A wrapper method to hide the actual test sequence away from the main code
     * @param n number of graph vertices to be generated for the particular test
     * @param m number of graph arcs to be generated for the particular test
     */
    public void run_test_04(int n, int m) {
        hr();
        System.out.println("TEST04: converting back and forth **twice**");
        System.out.println("\t\tNB! only significant operations are visualized:");
        Graph g13 = new Graph("G13");
        g13.createRandomSimpleGraph(n, m);
        SquareMatrix ma = new SquareMatrix();
        ma.setSize(n);
        ma.setId(g13.id);
        ma.content = g13.createAdjMatrix();
        System.out.println();
        Graph g14 = ma.toGraph("content");
        g14.id = "G14";
        System.out.println(g14);
        SquareMatrix mb = new SquareMatrix();
        mb.setSize(n);
        mb.content = g14.createAdjMatrix();
        mb.setId("G15");
        System.out.println();
        Graph g15 = mb.toGraph("content");
        g15.id = "G16";
        System.out.println(g15);
        System.out.println("The conclusion: methods: Graph.createAdjMatrix() and SquareMatrix.toGraph()");
        System.out.println("\tare complementary operations if used reversibly.");
        System.out.println("\tThe source of irregularity is Graph.createRandomSimpleGraph(n, m)\n");
    }

    /**
     * A wrapper method to hide the actual test sequence away from the main code
     * @param n number of graph vertices to be generated for the particular test
     * @param m number of graph arcs to be generated for the particular test
     */
    public void run_test_05(int n, int m) {
        hr();
        System.out.println("TEST05: Squaring and counting the elements of an adjacency matrix");
        System.out.println("\tit reflects the count of Arcs for both normal and squared matrices.");
        Graph ga = new Graph("G55");
        ga.createRandomSimpleGraph(n, m);
        ga.size = n;
        System.out.println(ga);

        SquareMatrix mm = new SquareMatrix();
        mm.setSize(ga.size);
        mm.setId(ga.id);
        mm.initAdjacencyMatrix();
        System.out.println(mm.id + " adj-matrix");
        mm.content = ga.createAdjMatrix(); //size will be deducted from the source
        mm.printOut("content");
        int sum1 = mm.sumOfElements("content");
        System.out.println("The count of elements in original adj-matrix is: " + sum1 + ".\n");

        System.out.println(mm.id + "^2 adj-matrix");
        mm.initSquaredMatrix(); // enforce the Vertex count for the resource
        mm.squareOfMatrix(); // MAIN operation - squaring the adj.matrix via dot multiplication
        mm.printOut("squared");
        int sum2 = mm.sumOfElements("squared");
        System.out.println("The count of elements in squared adj-matrix is: " + sum2 + ".");
        System.out.println("\tPlease note: 1) loops are missing from the main diagonal.");
        System.out.println("\t2) the properties of randomly generated graphs vary by instance.\n");
    }

    /**
     * A wrapper method to hide the actual test sequence away from the main code
     * @param n number of graph vertices to be generated for the particular test
     * @param m number of graph arcs to be generated for the particular test
     */
    public void run_test_06(int n, int m) {
        hr();
        // ======================= stage A - preparing a new adj.-list graph
        System.out.println("TEST06: Verify the results from both methods are identical.");
        System.out.println();
        Graph g = new Graph("G-initial");
        g.createRandomSimpleGraph(n, m);
        // g.size = n;
        System.out.println(g);
        // ======================= stage B - visualizing the adj-matrix
        SquareMatrix mm = new SquareMatrix();
        mm.setSize(g.size);
        mm.setId(g.id);
        System.out.println(mm.id + " --> adj-matrix");
        mm.content = g.createAdjMatrix();
        mm.printOut("content");
        int sum1 = mm.sumOfElements("content");
        System.out.println("The count of elements in this adj-matrix is: " + sum1 + ".");
        System.out.println();
        // ======================= Stage C.1 - Arc generation via WFI
        Graph gSquare = g.traversalVerbose(); // The actual BFS traversal
        // ======================= Stage C.2 - visualizing the adj-matrix of the square
        System.out.println(gSquare);
        System.out.println();
        System.out.println("Adj. matrix filled using matrix dot multiplication");
        System.out.println(mm.id + "--> adj-matrix --> dot multiplication");
        mm.initSquaredMatrix(); // apply the size  to another important class resource
        // ======================= Stage D - Arc generation via matrix multiplication
        mm.squareOfMatrix(); // The actual dot multiplication operation
        mm.printOut("squared");
        int sum2 = mm.sumOfElements("squared");
        System.out.println("The count of elements in this squared adj-matrix is: " + sum2 + ".");
        System.out.println();
        // ======================= Stage E - converting WFI created graph for comparison
        SquareMatrix xx = new SquareMatrix();
        xx.setSize(gSquare.size);
        xx.setId("G-squared-by-BFS-and-then-imported-into-the-Adj-Matrix");
        System.out.println(xx.id);
        xx.content = gSquare.createAdjMatrix(); //size will be deducted from the original adj.list
        xx.printOut("content"); // b/c we squared it externally
        int sum3 = xx.sumOfElements("content");
        System.out.println("The count of elements in this imported adj-matrix is: " + sum3 + ".");
        System.out.println();
    }

    /**
     * A wrapper script to hide the actual test sequence away from the main code
     * Speed test for method no 1 (dot multiplication of adj. matrix to itself)
     * @param n number of graph vertices to be generated for the particular test
     * @param m number of graph arcs to be generated for the particular test
     */
    public void calculate_PowerOfAGraph_viaMatrixMult(int n, int m) {
        hr();
        System.out.println("TEST07 (prefinal): Speed test for slow dot-mult. method (expect 30 sec)\n");
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(n, m);
        g.size = n;
        SquareMatrix mm = new SquareMatrix();
        mm.setSize(g.size);
        mm.setId(g.id);
        mm.content = g.createAdjMatrix();
        mm.initSquaredMatrix();
        System.out.println("Calculation started with n=" + n + " and m=" + m);
        System.out.println("\t\tstill running...");
        long timerStart = System.nanoTime();
        mm.squareOfMatrix();
        long timerStop = System.nanoTime();
        long mil = 1000000;
        System.out.println("READY. Time spent: " + (timerStop / mil - timerStart / mil) + " milliseconds.");
        Graph gg = new Graph("G^");
        gg = mm.silentToGraph("squared");
        int sum2 = mm.sumOfElements("squared");
        System.out.println("The count of elements in the squared adj-matrix is: " + sum2 + ".\n");
    }

    /**
     * A wrapper script to hide the actual test sequence away from the main code
     * Speed test for method no 2 (WFI style BFS traversal)
     * @param n number of graph vertices to be generated for the particular test
     * @param m number of graph arcs to be generated for the particular test
     */
    public void calculate_PowerOfAGraph_viaBFS(int n, int m) {
        hr();
        System.out.println("TEST08 (final): Speed test for fast method (BFS traversal)\n");
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(n, m);
        System.out.println("Calculation started with n=" + n + " and m=" + m);
        System.out.println("\t\tstill running...");
        long timerStart = System.nanoTime();
        Graph g2 = g.traversalSilent();
        long timerStop = System.nanoTime();
        long mil = 1000000;
        System.out.println("READY. Time spent: " + (timerStop / mil - timerStart / mil) + " milliseconds.");
    }

    /**
     * An original class to support the matrix multiplications
     * It contains two matrices (one for the adjacency matrix and another
     * for the power products.
     * @author jeekim616
     */
    class SquareMatrix {
        private int size;
        private String id;
        private int[][] content; // src adjacency matrix
        private int[][] squared; // dst adjacency matrix  after being squared

        @Override
        public String toString() {
            return id;
        }

        public void setSize(int s) {
            size = s;
        }

        public void setId(String ident) {
            id = ident;
        }

        /**
         * A wrapper method to form the matrix dimension
         */
        public void initAdjacencyMatrix() {
            int[][] adj = new int[size][size];
            this.content = adj;
        }

        /**
         * A wrapper method to form the matrix dimension
         */
        public void initSquaredMatrix() {
            int[][] squ = new int[size][size];
            this.squared = squ;
        }

        /**
         * The method calculates the power 2 of a graph via square of its
         * adjacency matrix (dot multiplication of matrices)
         * @author jeekim616
         * Inspiration:
         * // https://stackoverflow.com/questions/17623876/matrix-multiplication-using-arrays
         * // https://study.com/academy/lesson/multiplying-matrices-in-java.html
         * // https://www.coursera.org/lecture/advanced-data-structures/support-multiplying-adjacency-matrices-38zHi
         */
        public void squareOfMatrix() {
            id = id + "^2";
            for (int x = 0; x < size; x++) {
                for (int y = 0; y < size; y++) {
                    // true=1, false=0
                    boolean sumOfProducts = false;
                    for (int z = 0; z < size; z++) {
                        boolean productOfPair = false;
                        if ((content[x][z] * content[y][z]) == 1) productOfPair = true;
                        sumOfProducts = sumOfProducts | productOfPair; // logical OR
                    }
                    // Exclude the main diagonal (i.e. Arc loops)
                    if (sumOfProducts && (x != y)) this.squared[x][y] = 1;
                }
            }
        }

        /**
         * This test/debug tool sums all the elements of a matrix,
         * either this.content or this.squared, as requested by the parameter
         * @author jeekim616
         * @param word [content | squared]
         * @return arithmetical (!) sum of the elements in that matrix
         */
        public int sumOfElements(String word) {
            int[][] tmp = new int[size][size];
            if (word == "content") {
                tmp = content;
            } else tmp = squared;
            int sum = 0;
            for (int x = 0; x < size; x++) {
                for (int y = 0; y < size; y++) sum += tmp[x][y];
            }
            return sum;
        }

        /**
         * This test/debug tool sums all the elements of a matrix,
         * either this.content or this.squared, as requested by the parameter
         * @param word a choice from keywords [content | squared]
         * @author jeekim616
         */
        public void printOut(String word) {
            int[][] tmp = new int[size][size];
            if (word == "content") {
                tmp = content;
            } else tmp = squared;
            for (int x = 0; x < size; x++) {
                System.out.print("| ");
                for (int y = 0; y < size; y++)
                    System.out.print("r" +
                            (x + 1) + "*c" + (y + 1) + "=" + tmp[x][y] + " | ");
                System.out.println();
            }
        }

        /**
         * Method for conversion from the adjacency matrix representation
         * to the adjacency list representation
         * @author jeekim616 with strong inspiration from this skeleton
         * @param word a choice from keywords [content | squared]
         * @return pointer to the resulting graph
         */
        public Graph toGraph(String word) {
            int[][] tmp = null;
            if (word == "content") {
                tmp = content;
            } else tmp = squared;
            // Form an initial array for Vertex addressing
            Graph qc = new Graph(this.id);
            System.out.println(this.id);
            qc.first = null;
            qc.createVertexLine(this.size);
            Vertex[] vv = new Vertex[this.size];
            Vertex v = qc.first;
            int c = 0;
            while (v != null) {
                vv[c] = v;
                c++;
                v = v.next;
            }
            // Form the adj. list representation according to the adj. matrix
            for (int i = 0; i < this.size; i++) {
                System.out.print("r=" + (i + 1) + " | ");
                for (int j = 0; j < this.size; j++) {
                    int n = tmp[i][j];
                    while (n != 0) {
                        Vertex vi = vv[i];
                        Vertex vj = vv[j];
                        String arcDescription = "a" + vi.toString() + "_" + vj.toString();
                        qc.createArc(arcDescription, vi, vj);
                        System.out.print("c" + (j + 1) + "=" + n + " ");
                        n--;
                    }
                }
                System.out.println();
            }
            return qc;
        }

        /**
         * The **silent** version of the conversion method from the adjacency
         * matrix representation to the adjacency list representation
         * @author jeekim616 with strong inspiration from this skeleton
         * @param word a choice from keywords [content | squared]
         * @return pointer to the resulting graph
         */
        public Graph silentToGraph(String word) {
            int[][] tmp = null;
            if (word == "content") {
                tmp = content;
            } else tmp = squared;
            Graph qc = new Graph(this.id);
            qc.first = null;
            qc.createVertexLine(this.size);
            Vertex[] vv = new Vertex[this.size];
            Vertex v = qc.first;
            int c = 0;
            while (v != null) {
                vv[c] = v;
                c++;
                v = v.next;
            }
            for (int i = 0; i < this.size; i++) {
                for (int j = 0; j < this.size; j++) {
                    int n = tmp[i][j];
                    int k = 0;
                    while (n != 0) {
                        Vertex vi = vv[i];
                        Vertex vj = vv[j];
                        String arcDescription = "a" + vi.toString() + "_" + vj.toString() + "_" + k;
                        qc.createArc(arcDescription, vi, vj);
                        n--;
                        k++;
                    }
                }
            }
            return qc;
        }
    }

    /**
     *  Class for describing the Vertex in a Graph
     *  A vertex represents one node in a graph
     *  @author of the original skeleton: jpoial
     *   - https://bitbucket.org/itc_algorithms/home6/
     */
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        Vertex (String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex (String s) {
            this (s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

    }

    /**
     *  The class for describing an Arc (edge) in a Graph and providing
     *  methods to manipulate it.
     *  @author of the original skeleton: jpoial
     *  - https://bitbucket.org/itc_algorithms/home6/
     */
    class Arc {
        private String id;
        private Vertex target;
        private Arc next;

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }

    /**
     * The class to describing a Graph and providing methods to
     * manipulate it (add elements like Vertices and Arcs)
     * @author of the original skeleton: jpoial
     *  - https://bitbucket.org/itc_algorithms/home6/
     *  @author of amendments and further development jeekim616
     */
    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;
        private int size = 0;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        /**
         * A method to get the String representation of the graph
         * @author of the original skeleton: jpoial
         * @return the string to print out
         */
        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        /**
         * Method to create Vertices within the graph structure
         * @author of the original skeleton: jpoial
         * @param vid name for the Vertex to be created
         * @return pointer to the created Vertex
         */
        public Vertex createVertex(String vid) {
            Vertex result = new Vertex(vid);
            result.next = this.first;
            this.first = result;
            return result;
        }

        /**
         * Method to create Vertices within the graph structure
         * @author of the original skeleton: jpoial
         * @param aid text for the Arc name
         * @param from points to the origination Vertex
         * @param to points to the destination Vertex
         * @return pointer to the created Arc
         */
        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         * @author of the original skeleton: jpoial
         * with two tiny mods by jeekim616 (commented)
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0) return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + (n - i)); // permissive
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } // There is *no else*
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         * @author of the original skeleton: jpoial
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         * @author of the original skeleton: jpoial
         *    except a tiny mod to imply graph size and comments by jeekim616
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n); // n-1 edges created here
            this.size = n; // important mod by jeekim616
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * The method counts the number of vertices in the graph. It may
         * be considered as a debug tool for the experiments.
         *
         * @return the count of vertices discovered within the Graph
         * @author jeekim616
         */
        int countOfVertices() {
            Vertex v = this.first; // from the Header to the first Vertex
            int i = 0;
            while (v != null) {
                i++;
                v = v.next;
            }
            return i;
        }

        /**
         * The method counts the number of arcs over all vertices of the graph.
         * It may considered as a debug tool for the experiments.
         * As a byproduct, it creates a console printout of the graph structure
         * @return the count of the Arcs discovered in the Graph
         * @author jeekim616
         */
        int countOfArcsTotal() {
            Vertex v = this.first; // from the Header to the first Vertex
            int j = 0;
            int r = 0;
            while (v != null) {
                r++;
                System.out.print("v" + r + " | ");
                if (v.first != null) {
                    Arc a = v.first; // From the Vertex to an Arc
                    while (a != null) {
                        j++;
                        System.out.print(a.id + " ");
                        a = a.next; // From the Arc to an Arc
                    }
                }
                v = v.next;
                System.out.println();
            }
            return j;
        }

        /**
         * A method to create a consistent line of vertices needed
         * for the Arcs structure
         * @author slightly adapted by jeekim616
         * @param n lenght of the line (equal to the count of vertices)
         */
        //  inspiration: from https://bitbucket.org/RUusjarv/homework6/
        public void createVertexLine(int n) {
            if (n <= 0) return; // obviously a dot is not a line
            Vertex[] vertexLine = new Vertex[n];
            for (int i = 0; i < n; i++) {
                vertexLine[i] = createVertex("v" + (n - i)); // permissive
            }
        }
        
        /**
         * This method (a verbose version) is used to calculate the square of a graph
         * represented by an adjacency list.
         * Unevident conditions: no loops, no duplicates!
         * @return the graph squared.
         * @author jeekim616, valuable ideas from M.O. and J.P. and coffee and time.
         */
        public Graph traversalVerbose() {
            // The source Graph contained within (this) Class
            int s = this.size;
            Graph dstG = new Graph(this.id + "^2"); // The destination Graph
            dstG.size = s;
            dstG.first = null; // has no Vertexes so far
            Vertex[] vertexLine = new Vertex[s]; // an auxiliary Array to address Vertices directly
            System.out.println("DEBUG - There are " + s + " vertices in the original Graph: ");
            System.out.println("Info: Prepare a directly addressable array for Vertex fastening:\n");
            for (int i = 0; i < s; i++) {
                Vertex tempV = new Vertex("v" + (s - i)); // create the last V first - permissive
                vertexLine[(s - i - 1)] = tempV; // create the last Vertice first
                tempV.next = dstG.first; // insert the newly created structure...
                dstG.first = tempV; // ... between the Graph Header and the first Vertex
            }
            System.out.println("DEBUG - The collection array is holding a total of " + s + " vertices.");
            // Here the main cycle starts
            Vertex vx = this.first; // Transition from the Header to the first Vertex
            Vertex originalSrcVertex = vx; // Memorize the value for another cycle
            // Discover the two-edge sequences (== the underlying condition of square operation)
            System.out.println("Info: The power 2 traversal starts now.");
            System.out.println("\tdiscovering potential 2-hop arcs (including duplicates!):");
            int matchCount = 0;
            int realCount = 0;
            vx = originalSrcVertex;
            // verbally explicit endpoints for the two discovered arcs ("empty" makes errors visible)
            Vertex startAX = new Vertex("empty"); // starting point of the first arc (dimension X)
            Vertex endAX = new Vertex("empty"); // endpoint of the first arc
            Vertex startAY = new Vertex("empty"); // starting point of the second arc (dimension Y)
            Vertex endAY = new Vertex("empty"); // endpoint of the second arc
            ArrayList<String> intents = new ArrayList<String>();
            // dimension X
            while (vx != null) {
                startAX = vx;
                if (vx.first != null) {
                    Vertex vy = originalSrcVertex;
                    // dimension Y
                    while (vy != null) {
                        startAY = vy;
                        if (vy.first != null) {
                            Arc ax = vx.first; // Transition from the Vertex to an adjacent Arc
                            while (ax != null) {
                                endAX = ax.target;
                                Arc ay = vy.first; // Transition from the Vertex to an adjacent Arc
                                while (ay != null) {
                                    endAY = ay.target;
                                    // This is a magic exclusion formula, only proper Arcs will remain
                                    if ((endAX == startAY) && (startAX != endAY) && (startAX != startAY)) {
                                        // PAYLOAD: Generating extra Arcs to the Source Graph
                                        matchCount++;
                                        String arcStart = startAX.toString(); // 3x verbally explicit names
                                        String arcMid = endAX.toString();
                                        String arcEnd = endAY.toString();
                                        System.out.println(arcStart + "-" + arcMid + "-" + arcEnd);
                                        String arcID = "a" + arcStart + "_" + arcEnd;
                                        if (!intents.contains(arcID)) intents.add(arcID); // exclude loops
                                    }
                                    ay = ay.next; // Transition from the Arc to a next Arc (if any)
                                }
                                ax = ax.next; // Transition from the Arc to a next Arc (if any)
                            } // AX
                        } // VY
                        vy = vy.next;
                    } // While Y
                }
                vx = vx.next;
            } // While X
            // Now materializing the Arc intents
            Collections.sort(intents); // not required but still nice
            Iterator<String> list = intents.iterator();
            while (list.hasNext()) {
                realCount++;
                String aName = list.next();
                StringTokenizer st = new StringTokenizer(aName, "av_"); // String chemistry
                int ss = Integer.parseInt(st.nextToken());
                int dd = Integer.parseInt(st.nextToken());
                dstG.createArc(aName, vertexLine[(ss - 1)], vertexLine[(dd - 1)]);
            }
            System.out.println("\n\nInfo: We squared to an empty Vertex line with " + 0 + "/" + 0 / 2 + " arcs.");
            System.out.println("Info: Together with the duplicates, there were " +
                    matchCount + "/" + matchCount / 2 + " arc intents.");
            System.out.println("Info: We excluded duplicates, we executed only " +
                    realCount + "/" + realCount / 2 + " arc intents.");
            int fullArc = (s * (s - 1));
            System.out.println("A factoid: a complete graph with " + s +
                    " vertices would assume " + fullArc + "/" + (fullArc / 2) + " arcs.");
            return dstG;
        }

        /**
         * This method (a verbose version) is used to calculate the square of a graph
         * represented by an adjacency list.
         * Unevident conditions: no loops, no duplicates!
         * @return the graph squared.
         * @author jeekim616, valuable ideas from M.O. and J.P. and coffee and time.
         */
        public Graph traversalSilent() {

            int s = this.size;
            Graph dstG = new Graph(this.id + "^2"); // The destination Graph
            dstG.size = s;
            dstG.first = null; // has no Vertexes so far
            Vertex[] vertexLine = new Vertex[s]; // an auxiliary Array to address Vertices directly
            for (int i = 0; i < s; i++) {
                Vertex tempV = new Vertex("v" + (s - i)); // create the last V first
                vertexLine[(s - i - 1)] = tempV; // create the last Vertice first
                tempV.next = dstG.first; // insert the newly created structure...
                dstG.first = tempV; // ... between the Graph Header and the first Vertex
            }
            Vertex vx = this.first; // Transition from the Header to the first Vertex
            Vertex originalSrcVertex = vx; // Memorize the value for another cycle
            // Discover the two-edge sequences (== the underlying condition of square operation)
            int matchCount = 0;
            vx = originalSrcVertex;
            // verbally explicit endpoints for the two discovered arcs ("empty" makes errors visible)
            Vertex startAX = new Vertex("empty"); // starting point of the first arc (dimension X)
            Vertex endAX = new Vertex("empty"); // endpoint of the first arc
            Vertex startAY = new Vertex("empty"); // starting point of the second arc (dimension Y)
            Vertex endAY = new Vertex("empty"); // endpoint of the second arc
            ArrayList<String> intents = new ArrayList<String>();
            // dimension X
            while (vx != null) {
                startAX = vx;
                if (vx.first != null) {
                    Vertex vy = originalSrcVertex;
                    // dimension Y
                    while (vy != null) {
                        startAY = vy;
                        if (vy.first != null) {
                            Arc ax = vx.first; // Transition from the Vertex to an adjacent Arc
                            while (ax != null) {
                                endAX = ax.target;
                                Arc ay = vy.first; // Transition from the Vertex to an adjacent Arc
                                while (ay != null) {
                                    endAY = ay.target;
                                    // This is a magic exclusion formula, only proper Arcs will remain
                                    if ((endAX == startAY) && (startAX != endAY) && (startAX != startAY)) {
                                        // PAYLOAD: Generating extra Arcs to the Source Graph
                                        matchCount++;
                                        String arcStart = startAX.toString(); // 3x verbally explicit names
                                        String arcEnd = endAY.toString();
                                        String arcID = "a" + arcStart + "_" + arcEnd;
                                        if (!intents.contains(arcID)) intents.add(arcID); // exclude loops
                                    }
                                    ay = ay.next; // Transition from the Arc to a next Arc (if any)
                                }
                                ax = ax.next; // Transition from the Arc to a next Arc (if any)
                            } // AX
                        } // VY
                        vy = vy.next;
                    } // While Y
                }
                vx = vx.next;
            } // While X
            // Executing the Arc intents
            Collections.sort(intents); // not required but still nice
            Iterator<String> list = intents.iterator();
            while (list.hasNext()) {
                String aName = list.next();
                StringTokenizer st = new StringTokenizer(aName, "av_"); // String chemistry
                int ss = Integer.parseInt(st.nextToken());
                int dd = Integer.parseInt(st.nextToken());
                dstG.createArc(aName, vertexLine[(ss - 1)], vertexLine[(dd - 1)]);
            }
            return dstG;
        }
    }
}
